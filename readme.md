PROBLEM STATEMENT:
Consume -  https://www.alphavantage.co/query?function=TIME_SERIES_DAILY_ADJUSTED&apikey=NQCFKOVGZASY3EZ9&symbol=MSFT
Where symbol = MSFT is company name and is configurable.
Below are few example of company names - 
1) MSFT
2) ABB
3) AAL            AMERICAN AIRLINES GROUP INC
4) AAPL            APPLE INC
5) DELL            DELL TECHNOLOGIES INC
  
This API will return Time Series data per date of given company. 
 
Write function / webservice to crawl this API which returns json output and insert into database per company, per date data of all 8 factors.
 
Perform below operations on already consumed data - 
Write a function / Expose a Web Service which return all opening, closing, high, low for given company and date, also write this output to csv and json file.
Write a function / Expose a Web Service which will return per date difference between closing and opening value of a given company.
Write a function / Expose a Web Service which will calculate per date average of all companies' difference between closing and opening value. 
Write a function / Expose a Web Service which will return maximum number of continuous days where closing value was greater than opening value(where trend is positive) for a given company. 
*************************************************************************************************************************************************************************

Essentials:
1. Windows OS
2. MySql WorkBench

The cookie cutter template with the required code is present in this repo. Below is a description of each file prsent:

{{cookiecutter.directory_name}} - Main folder where all code and input/output data is set up
cookiecutter.json - Contains all default names for the ginger templates

CODE:
1. {{cookiecutter.config}}.ini - Contains credentials required to connect to database and details API provided in the assignment 
2. {{cookiecutter.setup_log_file}}.py - Contains Python code for configuring logger file, which is later used for debugging
3. {{cookiecutter.db_file_name}}.py - Contains Python code for connecting to MySql database
4. {{cookiecutter.data_load_file}}.py - Creates Table in above database if required
5. {{cookiecutter.queries_file}}.py - Contains MySql queries used to fetch data from above table
6. {{cookiecutter.main}}.py - Contains Python code for the flask application and in turn makes call to database connection, data loading and queries file 

DATA:
{{cookiecutter.data_file_name}} - Folder for all data required in the project
{{cookiecutter.output_file}} - Folder for output files generated 
1.  {{cookiecutter.query1_output}}.csv & {{cookiecutter.query1_output}}.json - Stores data fetched for query 1
2. {{cookiecutter.log_file}}.log - Stores all logging messages generated while running the project

INPUT:
1. db-name : Name of database
2. db-status : true if database already exists, else false
3. table-name : Name of table 
4. table-status : true if table already exists, else false
5. company : Name of company whose data is to fetched
6. date : Date for which data is to be fetched (Must be in YYYY-MM-DD format)

Input data is passed as HTTP arguments. 
For displaying results of any query, first connection with the database is to be established.
e.g. http://127.0.0.1:5000/database?db-name=pyassignment&db-status=true&table-name=ts_data&table-status=true (Values after ? are arguments)
After this, any of the 4 queries may be called with the required inputs, query1 requires company and date as input & query 2 required only company as input. The other queries do not need any arguments.
e.g. http://127.0.0.1:5000/query1?company=MSFT&date=2021-08-20
OR, http://127.0.0.1:5000/query2?company=DELL
OR, http://127.0.0.1:5000/query3
OR, http://127.0.0.1:5000/query4

************************************************************************************************************************************************************************
Author: Megha Chakravorty