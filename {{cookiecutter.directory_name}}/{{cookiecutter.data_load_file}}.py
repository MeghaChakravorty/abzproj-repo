import requests
from configparser import ConfigParser
config = ConfigParser()
config.read('{{cookiecutter.config_file}}.ini')
from {{cookiecutter.setup_log_file}} import logger

class DataLoad: #Class to set establish database connection
    def __init__(self,mydb,mycursor,table,table_status):
        self.mydb = mydb
        self.mycursor = mycursor
        self.table = table
        self.table_status = table_status

    def load(self):
        if self.table_status.lower()=="false":
            url = config['data']['url'] #Acquires url from config file 
            companies = config['data']['companies'] #Acquires list of companies from config file 
        
            l = [] #list l to store all values from the API
            cnt=1 #Using cnt to add serial number to the data
        
            for c in companies:
                r = requests.get(url.format(c))
            data = r.json()

            try:
                for x in data["Time Series (Daily)"]:
                    l.append((cnt,
                            data["Time Series (Daily)"][x]["1. open"],
                            data["Time Series (Daily)"][x]["2. high"],
                            data["Time Series (Daily)"][x]["3. low"],
                            data["Time Series (Daily)"][x]["4. close"],
                            data["Time Series (Daily)"][x]["5. adjusted close"],
                            data["Time Series (Daily)"][x]["6. volume"],
                            data["Time Series (Daily)"][x]["7. dividend amount"],
                            data["Time Series (Daily)"][x]["8. split coefficient"],
                            x,
                            data["Meta Data"]["2. Symbol"]))
                    cnt+=1
            except:
                logger.info("Error while extracting data! (Temporary glitch)\n")
                return(False,"\nError while extracting data! (Temporary glitch)\n")
            try:
                #Creating table for data storage
                self.mycursor.execute(f"Create table {self.table} (slno int(10),open float(20), high float(20), low float(20), close float(20), adjusted_close float(20), volume int(20), dividend float(20), split_coef float(20), date_1 varchar(20) , company varchar(20))")
                sqlform = f"Insert into {self.table}(slno,open,high,low,close,adjusted_close,volume,dividend,split_coef,date_1,company) values(%s,%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
                self.mycursor.executemany(sqlform,l) #Runs sql statement passed as 1st argument as long as all the data in list l aren't covered
                self.mydb.commit()
            except: #Exception handling if new table name matches with exsisting table
                logger.info("Table with this name already exists!\n")
                return(False,"Table with this name already exists!\n")
        
        else:
            try: #Check if table exists
                self.mycursor.execute(f"Select COUNT(*) FROM {self.table}")
            except:
                logger.info("Given table doesn't exist!\n")
                return(False,"\nGiven table doesn't exist!\n")
        return(True,"Data loading successful")