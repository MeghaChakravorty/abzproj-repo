import mysql.connector
from configparser import ConfigParser
config = ConfigParser()
config.read('{{cookiecutter.config_file}}.ini')
from {{cookiecutter.setup_log_file}} import logger

class Database:
    def __init__(self,db_name,db_status):
        self.db_name = db_name
        self.db_status = db_status

    def connect(self):
        if self.db_status.lower()=="true": 
            try:
                #Credentials fetched from config file
                mydb = mysql.connector.connect(host = config['credentials']['host'],
                            user = config['credentials']['user'],
                            password = config['credentials']['pwd'],
                            database = self.db_name
                ) #credentials specific to user is passed
                mycursor = mydb.cursor(buffered=True) #.cursor function allows usage of MySql Statements in Python
            except:
                logger.info("ERROR: Incorrect credentials or Database doesn't exist\n")
                return(False,"\nERROR: Incorrect credentials or Database doesn't exist\n", None, None)
        else:
            try:
                #Credentials fetched from config file
                mydb = mysql.connector.connect(host = config['credentials']['host'],
                            user = config['credentials']['user'],
                            password = config['credentials']['pwd']
                )#credentials specific to user is passed
            except:
                return(False,"Incorrect credentials", None, None)
            mycursor = mydb.cursor(buffered=True)
            try:
                #Creation of required database if it doesn't already exist
                mycursor.execute(f"Create database {self.db_name}")
                mycursor.execute(f"Use {self.db_name}")
            except:
                logger.info("Database with this name already exists!\n")
                return(False,"\nDatabase with this name already exists!\n", None, None)
        
        return(True, "\nConnected to database\n", mydb, mycursor)