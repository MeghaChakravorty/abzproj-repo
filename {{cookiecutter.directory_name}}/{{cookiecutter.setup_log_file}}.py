import logging
from configparser import ConfigParser
config = ConfigParser()
config.read('{{cookiecutter.config_file}}.ini')
import os

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

formatter = logging.Formatter('%(asctime)s:%(name)s:%(filename)s:%(message)s')

file_handler = logging.FileHandler(os.getcwd()+'/{{cookiecutter.data_file_name}}/{{cookiecutter.log_file}}.log') #Sets location for storing log file
file_handler.setLevel(logging.DEBUG)
file_handler.setFormatter(formatter)

logger.addHandler(file_handler)
