import pandas as pd
from {{cookiecutter.setup_log_file}} import logger
import os 

class Query:
    def __init__(self, mycursor, table, company, date):
        self.mycursor = mycursor
        self.table = table
        self.company = company
        self.date = date
        self.path = os.getcwd()+'/{{cookiecutter.data_file_name}}/{{cookiecutter.output_file}}/{{cookiecutter.query1_output}}'
       
    def query1(self):
        #returns all opening, closing, high, low for given company and date, also writes this output to csv and json file
        self.mycursor.execute(f'Select company,date_1,open,close,high,low from {self.table} where date_1="{self.date}" AND company="{self.company}"')
        result = self.mycursor.fetchall() #fetches all rows obtained as output to above query
        df = pd.DataFrame(result, columns=["company","date","open","close","high", "low"]) #Converting list to pandas dataframe
        df.to_csv(self.path+'.csv',index=False) 
        df.to_json(self.path+'.json',orient='records')
        
        if df.empty:
            logger.info("\nNo data retrieved in query1\n")
        return(df.to_html(header="true", table_id="table"))    
        
    
    def query2(self):
        #returns per date difference between closing and opening value of a given company.
        self.mycursor.execute(f'Select company,date_1,(close-open) as difference from {self.table} where company="{self.company}"')
        result1 = self.mycursor.fetchall() #fetches all rows obtained as output to above query
        df_diff = pd.DataFrame(result1, columns=["company","date","difference"]) #Converting list to pandas dataframe 
        
        if df_diff.empty:
            logger.info("\nNo data retrieved in query2\n")
        return (df_diff.to_html(header="true", table_id="table"))
    
    
    def query3(self):
        #returns per date average of all companies' difference between closing and opening value. 
        self.mycursor.execute(f'Select date_1,AVG(close-open) as avg_diff from {self.table} GROUP BY date_1')
        result2 = self.mycursor.fetchall() #fetches all rows obtained as output to above query
        df_avg = pd.DataFrame(result2, columns=["date","average difference"]) #Converting list to pandas dataframe 
        if df_avg.empty:
            logger.info("\nNo data retrieved in query3\n")
        return(df_avg.to_html(header="true", table_id="table"))
    
    
    def query4(self):
        #returns maximum number of continuous days where closing value was greater than opening value(where trend is positive) for a given company. 
        self.mycursor.execute(f'Select company,date_1 from {self.table} WHERE close>open ORDER BY company, date_1 asc') #Sql query that returns all entries for days where closing value> opening value
        result3 = self.mycursor.fetchall()
        df_cd = pd.DataFrame(result3, columns=["company","date"]) #Converting list to pandas dataframe
        #Dataframe's date column first grouped by Company, then difference between current date and previous date is compared to 1 day using .gt
        #.cumsum calculates sum of all entries that returned True for above comparison
        m = (df_cd.assign(date=pd.to_datetime(df_cd['date'])) 
               .groupby('company')['date']
               .diff()
               .gt(pd.Timedelta('1D'))
               .cumsum())
        df_cd = df_cd.groupby(['company', m]).size().max(level='company')
        df_new = df_cd.to_frame().reset_index()
        df_new.rename(columns = {0:'max consecutive days'},inplace = True)
        if df_new.empty:
            logger.info("\nNo data retrieved in query4\n")
        return(df_new.to_html(table_id="table"))