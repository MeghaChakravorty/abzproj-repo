from {{cookiecutter.db_file_name}} import Database
from {{cookiecutter.data_load_file}} import DataLoad
from flask import Flask, request
from {{cookiecutter.queries_file}} import Query
from {{cookiecutter.setup_log_file}} import logger

app = Flask(__name__)

@app.route("/")
def home():
    return("Greetings {{cookiecutter.greeting_recipient}}!")

@app.route("/database")
def database():  
    global table
    global mydb, mycursor
    #Acquiring input from HTTP arguments
    db_name = request.args.get('db-name')
    db_status = request.args.get('db-status')
    table = request.args.get('table-name')
    table_status = request.args.get('table-status') 

    Db_obj = Database(db_name,db_status)
    flag_db, msg_db, mydb, mycursor = Db_obj.connect()
    if flag_db:
        dL = DataLoad(mydb, mycursor, table, table_status)
        flag_t,msg_t = dL.load()
    else: 
        return(msg_db)
    return(msg_db+msg_t)

@app.route("/query1")
def query1():   
    #Acquiring input from HTTP arguments
    company = request.args.get('company')
    date = request.args.get('date')
    try:
        Obj1 = Query(mycursor, table, company, date)
    except:
        logger.exception("Connection lost!")
        return("Connection lost!")
    return Obj1.query1()

@app.route("/query2")
def query2():
    #Acquiring input from HTTP arguments
    company = request.args.get('company')
    try:
        Obj2 = Query(mycursor, table, company, None)
    except:
        logger.exception("Connection lost!")
        return("Connection lost!")
    return Obj2.query2()

@app.route("/query3")
def query3():
    try:
        Obj3 = Query(mycursor, table, None, None)
    except:
        logger.exception("Connection lost!")
        return("Connection lost!")
    return Obj3.query3()

@app.route("/query4")
def query4():
    try:
        Obj4 = Query(mycursor, table, None, None) 
    except:
        logger.exception("Connection lost!")
        return("Connection lost!")
    return Obj4.query4()

    
if __name__ == "__main__":
    logger.debug("Logging...")
    app.run(debug=True)